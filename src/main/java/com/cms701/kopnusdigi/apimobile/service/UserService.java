package com.cms701.kopnusdigi.apimobile.service;

import com.cms701.kopnusdigi.apimobile.cons.RC;
import com.cms701.kopnusdigi.apimobile.model.mapping.User;
import com.cms701.kopnusdigi.apimobile.repository.TblUserRepository;
import com.cms701.kopnusdigi.apimobile.response.BaseResponse;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
@CommonsLog
public class UserService {

    @Autowired
    TblUserRepository userRepository;

    public BaseResponse<User> findById(String id) {
        log.info("Get users by : " + id);
        AtomicReference<User> user = new AtomicReference<>();
        userRepository.findById(id).ifPresent(e -> user.set(new User(e.getUserId(), e.getName(), e.getRegisterDate(), e.getStatus())));

        return user.get() == null ?
                new BaseResponse<>(RC.DATA_NOT_FOUND, "User "+id+" not found") :
                new BaseResponse<>(RC.SUCCESS, "Success", user.get());
    }

    public BaseResponse<List<User>> getAll(int page, int row) {
        log.info("Get all users");
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(e -> users.add(new User(e.getUserId(), e.getName(), e.getRegisterDate(), e.getStatus())));

        log.info("Total users : " + users.size());
        return new BaseResponse<>(RC.SUCCESS, "Success", users);
    }

}