package com.cms701.kopnusdigi.apimobile.exception;

public class SignatureException extends Exception {
    public SignatureException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
