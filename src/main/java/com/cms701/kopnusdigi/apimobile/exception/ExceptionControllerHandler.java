package com.cms701.kopnusdigi.apimobile.exception;

import com.cms701.kopnusdigi.apimobile.cons.RC;
import com.cms701.kopnusdigi.apimobile.response.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.xml.ws.Response;

@Controller
public class ExceptionControllerHandler {

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity SignatureException() {
        return ResponseEntity.ok().body(
                new BaseResponse(RC.FAILED, "Signature tidak valid !")
        );
    }

}
