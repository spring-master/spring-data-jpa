package com.cms701.kopnusdigi.apimobile.model.mapping;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class User {
    String userId;
    String name;
    Date registerDate;
    char status;
}
