package com.cms701.kopnusdigi.apimobile.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_book", schema = "public")
public class TblBook {
    @Id
    String id;
    String title;
    String publishDate;
    String genre;
}