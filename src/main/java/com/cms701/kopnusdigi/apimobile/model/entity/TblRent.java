package com.cms701.kopnusdigi.apimobile.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_rent", schema = "public")
public class TblRent {
    @Id
    String id;
    String bookId;
    String userId;
//    TblBook book;
//    TblUser user;
    Date startDate;
    Date finishDate;
}