package com.cms701.kopnusdigi.apimobile.request;

import lombok.Setter;
import lombok.Getter;

@Getter @Setter
public class DataRequest<T> extends BaseRequest {
    T data;
}