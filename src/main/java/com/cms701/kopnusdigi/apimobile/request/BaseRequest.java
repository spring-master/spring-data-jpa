package com.cms701.kopnusdigi.apimobile.request;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class BaseRequest {
    @JsonAlias("terminal_id")
    protected String sessionId;

    @JsonAlias("terminal_date")
    protected String terminalDate;

    @JsonAlias("hp_no")
    protected String phoneNumber;

    @JsonAlias("hp_ype")
    protected String phoneType;
}