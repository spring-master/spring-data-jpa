package com.cms701.kopnusdigi.apimobile.repository;

import com.cms701.kopnusdigi.apimobile.model.entity.TblUser;
import org.springframework.data.repository.CrudRepository;

public interface TblUserRepository extends CrudRepository<TblUser, String> {

}