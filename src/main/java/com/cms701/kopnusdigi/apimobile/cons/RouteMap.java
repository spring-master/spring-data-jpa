package com.cms701.kopnusdigi.apimobile.cons;

public class RouteMap {
    public static final String DASHBOARD = "/dashboard";
    public static final String DASHBOARD_BANNER = "/banner";
    public static final String DASHBOARD_PROMO = "/promo";
    public static final String DASHBOARD_MENU = "/menu";
    public static final String DASHBOARD_FAVORITE = "/favorite";
    public static final String DASHBOARD_USER = "/user";
}
