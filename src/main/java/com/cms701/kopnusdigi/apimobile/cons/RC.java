package com.cms701.kopnusdigi.apimobile.cons;

public class RC {
    public static final String SUCCESS = "00";
    public static final String FAILED = "01";
    public static final String INTERNAL_ERROR = "99";

    public static final String DATA_NOT_FOUND = "04";
}
