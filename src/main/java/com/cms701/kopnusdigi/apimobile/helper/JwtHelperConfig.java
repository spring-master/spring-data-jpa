package com.cms701.kopnusdigi.apimobile.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@lombok.Getter
@Component
public class JwtHelperConfig {
    @Value("${client.jwt.secret}")
    private String secret;
}
