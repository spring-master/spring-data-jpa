package com.cms701.kopnusdigi.apimobile.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Global {
    public static JwtHelper jwtHelper;

    @Autowired
    public Global (JwtHelper jwtHelper) {
        Global.jwtHelper = jwtHelper;
    }

}
