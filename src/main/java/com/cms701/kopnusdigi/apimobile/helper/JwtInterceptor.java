package com.cms701.kopnusdigi.apimobile.helper;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sun.nio.ch.IOUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@CommonsLog
//@Component
public class JwtInterceptor implements WebFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        log.info("Request " + exchange.getRequest().getURI().getPath() + " from " + exchange.getRequest().getRemoteAddress().getAddress());

        return exchange
                .getRequest()
                .getBody()
                .next()
                .flatMap(e -> {
                    try {
                        e.asOutputStream().write(e.asByteBuffer().array());
                        return chain.filter(exchange);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        return Mono.error(ex);
                    }
                });
    }

    protected byte[] addByte(byte[] data) {
        String value = String.valueOf(data);
        value = value + " : Edited";
        return value.getBytes();
    }

    protected DataBuffer decodeDataBuffer(DataBuffer dataBuffer) throws IOException {
        CharBuffer charBuffer = StandardCharsets.UTF_8.decode(dataBuffer.asByteBuffer());
        DataBufferUtils.release(dataBuffer);
        String value = charBuffer.toString() + " : Edited";
        dataBuffer.write(value.getBytes());
        dataBuffer.asOutputStream().write(value.getBytes());
        return dataBuffer;
    }

}
