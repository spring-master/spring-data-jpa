package com.cms701.kopnusdigi.apimobile.helper;

import com.cms701.kopnusdigi.apimobile.exception.SignatureException;
import lombok.extern.apachecommons.CommonsLog;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@CommonsLog
public class JwtHelper {
	private JwtHelperConfig config;
	private JsonWebSignature jws;
	private JwtConsumer jwtConsumer;

	@Autowired
	public JwtHelper(JwtHelperConfig config) {
		this.config = config;

		HmacKey secretKey = new HmacKey(config.getSecret().getBytes());

		// Encoder
		jws = new JsonWebSignature();
		jws.setKey(secretKey);
		jws.setDoKeyValidation(false);
		jws.setAlgorithmHeaderValue("HS256");

		// Decoder
		jwtConsumer = new JwtConsumerBuilder()
			.setVerificationKey(secretKey)
			.setRelaxVerificationKeyValidation()
			.build();
	}

	public synchronized String encode(String plain) throws SignatureException {
		try {
			jws.setPayload(plain);
			return jws.getCompactSerialization();
		} catch (JoseException e) {
			throw new SignatureException(e.getMessage(), e);
		}
	}
	
	public synchronized String decode(String encoded) throws SignatureException {
		try {
			return jwtConsumer.processToClaims(encoded).toJson();
		} catch (InvalidJwtException e) {
			log.error(e.getMessage(), e);
			throw new SignatureException(e.getMessage(), e);
		}
	}
	
}