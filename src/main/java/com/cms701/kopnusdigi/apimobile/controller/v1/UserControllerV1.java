package com.cms701.kopnusdigi.apimobile.controller.v1;

import com.cms701.kopnusdigi.apimobile.cons.Default;
import com.cms701.kopnusdigi.apimobile.model.mapping.User;
import com.cms701.kopnusdigi.apimobile.response.BaseResponse;
import com.cms701.kopnusdigi.apimobile.response.PageResponse;
import com.cms701.kopnusdigi.apimobile.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.List;

@RestController
@RequestMapping("/v1/user")
public class UserControllerV1 {

    @Autowired
    UserService userService;

    @GetMapping(value = "/{id}")
    public BaseResponse<User> get(@PathVariable String id) {
        return userService.findById(id);
    }

    @GetMapping
    public BaseResponse<List<User>> get(
            @RequestParam(defaultValue = Default.DEFAULT_PAGE) int page,
            @RequestParam(defaultValue = Default.DEFAULT_ROW) int row) {
        return userService.getAll(page, row);
    }

}