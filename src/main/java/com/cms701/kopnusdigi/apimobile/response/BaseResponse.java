package com.cms701.kopnusdigi.apimobile.response;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BaseResponse<T> {
    String rc;
    String desc;
    T data;

    public BaseResponse(String rc, String desc) {
        this.rc = rc;
        this.desc = desc;
    }
}