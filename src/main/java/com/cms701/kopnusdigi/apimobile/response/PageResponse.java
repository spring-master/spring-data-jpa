package com.cms701.kopnusdigi.apimobile.response;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@lombok.Getter
public class PageResponse<T> extends BaseResponse<List<T>> {
    Pageable page;

    public PageResponse(String rc, String desc, List<T> data, Pageable page) {
        super(rc, desc, data);
        this.page = page;
    }
}
