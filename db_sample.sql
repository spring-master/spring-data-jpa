/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 90521
 Source Host           : localhost:5433
 Source Catalog        : db_sample
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90521
 File Encoding         : 65001

 Date: 27/02/2020 16:52:15
*/


-- ----------------------------
-- Table structure for tbl_book
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_book";
CREATE TABLE "public"."tbl_book" (
  "id" varchar(17) COLLATE "pg_catalog"."default" NOT NULL DEFAULT to_char(now(), 'yyyyMMddHHmmssSSS'::text),
  "title" varchar(50) COLLATE "pg_catalog"."default",
  "publish_date" varchar(4) COLLATE "pg_catalog"."default",
  "genre" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."tbl_book" OWNER TO "postgres";

-- ----------------------------
-- Records of tbl_book
-- ----------------------------
BEGIN;
INSERT INTO "public"."tbl_book" VALUES ('2020022603024545S', 'Sukses Dengan Binomo', '2020', 'Finance');
INSERT INTO "public"."tbl_book" VALUES ('2020022603021111S', 'Aispul Jameel Sang Idol', '2019', 'Music, Entertainment');
INSERT INTO "public"."tbl_book" VALUES ('2020022603020404S', 'Culametan Met Met', '2020', 'Komedi');
INSERT INTO "public"."tbl_book" VALUES ('2020022603022424S', 'You and Me', '2018', 'Romance');
COMMIT;

-- ----------------------------
-- Table structure for tbl_rent
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_rent";
CREATE TABLE "public"."tbl_rent" (
  "id" varchar(17) COLLATE "pg_catalog"."default" NOT NULL DEFAULT to_char(now(), 'yyyyMMddHHmmssSSS'::text),
  "book_id" varchar(17) COLLATE "pg_catalog"."default" NOT NULL DEFAULT to_char(now(), 'yyyyMMddHHmmssSSSS'::text),
  "user_id" varchar(50) COLLATE "pg_catalog"."default",
  "start_date" timestamp(6) DEFAULT now(),
  "finish_date" timestamp(6) DEFAULT now()
)
;
ALTER TABLE "public"."tbl_rent" OWNER TO "postgres";

-- ----------------------------
-- Records of tbl_rent
-- ----------------------------
BEGIN;
INSERT INTO "public"."tbl_rent" VALUES ('2020022603024747S', '2020022603024545S', 'abdul.aris@kopnus.com', '2020-02-26 15:53:16', '2020-02-26 15:53:12');
INSERT INTO "public"."tbl_rent" VALUES ('2020022603025555S', '2020022603024545S', 'abdul.aris@kopnus.com', '2020-02-26 15:53:16', '2020-02-26 15:53:12');
INSERT INTO "public"."tbl_rent" VALUES ('2020022603025959S', '2020022603024545S', 'abdul.aris@kopnus.com', '2020-02-26 15:53:16', '2020-02-26 15:53:12');
INSERT INTO "public"."tbl_rent" VALUES ('2020022603025252S', '2020022603024545S', 'nur.kholik@outlook.com', '2020-02-26 15:53:16', '2020-02-26 15:53:12');
INSERT INTO "public"."tbl_rent" VALUES ('2020022603025353S', '2020022603020404S', 'abdul.aris@kopnus.com', '2020-02-26 15:53:16', '2020-02-26 15:53:12');
INSERT INTO "public"."tbl_rent" VALUES ('2020022603025757S', '2020022603022424S', 'ridwan@binomo.com', '2020-02-26 15:53:16', '2020-02-26 15:53:12');
INSERT INTO "public"."tbl_rent" VALUES ('2020022603025454S', '2020022603024545S', 'ridwan@binomo.com', '2020-02-26 15:53:16', '2020-02-26 15:53:12');
COMMIT;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_user";
CREATE TABLE "public"."tbl_user" (
  "user_id" varchar(25) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(50) COLLATE "pg_catalog"."default",
  "register_date" timestamp(6) DEFAULT now(),
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '1'::bpchar
)
;
ALTER TABLE "public"."tbl_user" OWNER TO "postgres";

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
BEGIN;
INSERT INTO "public"."tbl_user" VALUES ('nur.kholik@outlook.com', 'Nur Kholik', '2020-02-26 15:51:52.142527', '1');
INSERT INTO "public"."tbl_user" VALUES ('abdul.aris@kopnus.com', 'Aris Culametan', '2020-02-26 15:52:08.433396', '1');
INSERT INTO "public"."tbl_user" VALUES ('ridwan@binomo.com', 'Ridwan Binomo', '2020-02-26 15:52:23.364116', '1');
COMMIT;

-- ----------------------------
-- Primary Key structure for table tbl_book
-- ----------------------------
ALTER TABLE "public"."tbl_book" ADD CONSTRAINT "tbl_book_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tbl_rent
-- ----------------------------
ALTER TABLE "public"."tbl_rent" ADD CONSTRAINT "tbl_rent_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tbl_user
-- ----------------------------
ALTER TABLE "public"."tbl_user" ADD CONSTRAINT "tbl_user_pkey" PRIMARY KEY ("user_id");

-- ----------------------------
-- Foreign Keys structure for table tbl_rent
-- ----------------------------
ALTER TABLE "public"."tbl_rent" ADD CONSTRAINT "fk-rent-book" FOREIGN KEY ("book_id") REFERENCES "public"."tbl_book" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."tbl_rent" ADD CONSTRAINT "fk-rent-user" FOREIGN KEY ("user_id") REFERENCES "public"."tbl_user" ("user_id") ON DELETE RESTRICT ON UPDATE RESTRICT;
